package com.example.julian.tourissquare;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText TextEmail;
    private EditText TextPassword;
    private ProgressDialog progressDialog;
    //Declaro un objeto firebaseAuth
    private FirebaseAuth firebaseAuth;


    RelativeLayout rellay1,rellay2;
    Handler handler=new Handler();
    Runnable runnable=new Runnable() {
        @Override
        public void run() {
            rellay1.setVisibility(View.VISIBLE);
            rellay2.setVisibility(View.VISIBLE);
        }
    };




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        //inicializo el objeto firebaseAuth
        firebaseAuth = FirebaseAuth.getInstance();

        //Referenciamos los views
        TextEmail = (EditText) findViewById(R.id.TextEmail);
        TextPassword = (EditText) findViewById(R.id.TextPassword);

        Button btnIniciarSesion = (Button) findViewById(R.id.btnIniciarSesion);
        Button btnCrearCuenta=(Button) findViewById(R.id.btnCrearCuenta);
        Button btnOlvidar=(Button) findViewById(R.id.btnOlvidar);

        btnIniciarSesion.setOnClickListener(this);
        btnCrearCuenta.setOnClickListener(this);
        btnOlvidar.setOnClickListener(this);


        progressDialog = new ProgressDialog(this);


        rellay1=(RelativeLayout) findViewById(R.id.rellay1);
        rellay2=(RelativeLayout) findViewById(R.id.rellay2);
        handler.postDelayed(runnable,500); //tiempo de entrada




    }


    private void loginUsuario() {

        //Obtenemos el email y la contraseña desde las cajas de texto
        final String email = TextEmail.getText().toString().trim();
        String password = TextPassword.getText().toString().trim();

        //Verificamos que las cajas de texto no esten vacías
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, "Se debe ingresar un email", Toast.LENGTH_LONG).show();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, "Falta ingresar la contraseña", Toast.LENGTH_LONG).show();
            return;
        }


        progressDialog.setMessage("Realizando inicio de sesión...");
        progressDialog.show();

        //login User
        firebaseAuth.signInWithEmailAndPassword(email, password)

                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        FirebaseUser users = firebaseAuth.getCurrentUser();
                        if(users.isEmailVerified()){


                            if (task.isSuccessful()) {
                                int pos = email.indexOf("@");
                                String user = email.substring(0, pos);
                                Toast.makeText(MainActivity.this, "Bienvenido: " + TextEmail.getText(), Toast.LENGTH_LONG).show();
                                Intent intencion = new Intent(getApplication(), principal.class);
                                startActivity(intencion);
                            } else {
                                if (task.getException() instanceof FirebaseAuthUserCollisionException)
                                    Toast.makeText(MainActivity.this, "El usuario ya existe. ", Toast.LENGTH_LONG).show();
                                else
                                    Toast.makeText(MainActivity.this, "No se pudo iniciar sesión ", Toast.LENGTH_LONG).show();
                            }


                        }else{

                            Toast.makeText(MainActivity.this, "Verifique su cuenta", Toast.LENGTH_LONG).show();
                        }

                        progressDialog.dismiss();
                    }
                });
        }

    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.btnIniciarSesion:
                loginUsuario();
                break;
            case R.id.btnCrearCuenta:
                startActivity(new Intent(this, registro.class));
                break;
            case R.id.btnOlvidar:
                startActivity(new Intent(this, olvidarContrasenaActivity.class));
                break;
        }


    }

}
