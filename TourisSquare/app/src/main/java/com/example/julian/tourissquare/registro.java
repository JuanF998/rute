package com.example.julian.tourissquare;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

import com.google.firebase.auth.FirebaseUser;

public class registro extends AppCompatActivity implements View.OnClickListener {


    private EditText TextEmail;
    private EditText TextPassword;
    private EditText TextPassword2;
    private EditText TextName;
    private EditText TextID;
    private ProgressDialog progressDialog;
    //Declaro un objeto firebaseAuth
    private FirebaseAuth firebaseAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registro_ususario);


        //inicializo el objeto firebaseAuth
        firebaseAuth = FirebaseAuth.getInstance();

        //Referenciamos los views
        TextName = (EditText) findViewById(R.id.TxtNombre);
        TextEmail = (EditText) findViewById(R.id.TxtCorreo);
        TextPassword = (EditText) findViewById(R.id.TxtContrasena);
        TextPassword2 = (EditText) findViewById(R.id.TxtContrasena2);
       // TextID = (EditText) findViewById(R.id.TxtId);

        Button btnCrearCuenta = (Button) findViewById(R.id.btnRegistar);


        btnCrearCuenta.setOnClickListener(this);


        progressDialog = new ProgressDialog(this);
    }


    private void registrarUsuario() {

        //Obtenemos el email y la contraseña desde las cajas de texto
        final String email = TextEmail.getText().toString().trim();
        final String password1 = TextPassword.getText().toString().trim();
        String password2 = TextPassword2.getText().toString().trim();
       // final String id = TextID.getText().toString().trim();
        final String name = TextName.getText().toString().trim();
        //Verificamos que las cajas de texto no esten vacías
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, "Se debe ingresar un email", Toast.LENGTH_LONG).show();
            return;

        }

        int pos = email.indexOf("@");
        String dominio = email.substring(0, pos);
        dominio+="@upb.edu.co";

        if (!TextUtils.equals(email,dominio)) {

            Toast.makeText(this, "El dominio de la dirección de correo electrónico debe ser upb.edu.co", Toast.LENGTH_LONG).show();
            return;

        }


        if (TextUtils.isEmpty(password1)) {
            Toast.makeText(this, "Falta ingresar la contraseña", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(password2)) {
            Toast.makeText(this, "Falta ingresar la confirmación de la contraseña", Toast.LENGTH_LONG).show();
            return;
        }

        progressDialog.setMessage("Realizando registro en linea...");
        progressDialog.show();

        //creating a new user
        firebaseAuth.createUserWithEmailAndPassword(email, password1)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            String user_id = firebaseAuth.getCurrentUser().getUid();
                            FirebaseUser user = firebaseAuth.getCurrentUser();
                            DatabaseReference currentUserDb = FirebaseDatabase.getInstance().getReference().child("Users").child(user_id); //.child(user_id)
                            user.sendEmailVerification();

                            Map nuevaPubliacion = new HashMap();
                            nuevaPubliacion.put("nombre", name);
                            nuevaPubliacion.put("correo", email);
                         //   nuevaPubliacion.put("ID", id);

                            currentUserDb.setValue(nuevaPubliacion);


                            Toast.makeText(registro.this, "Se ha registrado el usuario con el email: " + TextEmail.getText(), Toast.LENGTH_LONG).show();
                            Intent intencion = new Intent(getApplication(), MainActivity.class);
                            startActivity(intencion);


                        } else {
                            if (task.getException() instanceof FirebaseAuthUserCollisionException)
                                Toast.makeText(registro.this, "El usuario ya existe. ", Toast.LENGTH_LONG).show();
                            else if (password1.length() < 6) {

                                Toast.makeText(registro.this, "La contraseña debe tener como mínimo 6 caracteres", Toast.LENGTH_LONG).show();


                            } else
                                Toast.makeText(registro.this, "No se pudo registrar el usuario ", Toast.LENGTH_LONG).show();
                        }
                        progressDialog.dismiss();
                    }
                });


    }

    private void enviarCorreoVerificacion() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            user.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        Toast.makeText(registro.this, "Check your Email for verification", Toast.LENGTH_SHORT).show();
                        FirebaseAuth.getInstance().signOut();
                    }
                }
            });
        }
    }

    public void onClick(View view) {


        registrarUsuario();


    }

}
