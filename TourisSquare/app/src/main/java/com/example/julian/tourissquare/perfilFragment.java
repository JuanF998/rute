package com.example.julian.tourissquare;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import de.hdodenhof.circleimageview.CircleImageView;


public class perfilFragment extends Fragment {
    @Nullable

    private final String CARPETA_RAIZ="misImagenesPrueba/";
    private final String RUTA_IMAGEN=CARPETA_RAIZ+"misFotos";

    final int COD_SELECCIONA=10;
    final int COD_FOTO=20;

    ImageView imagen;
    String path;


    private CircleImageView ProfileImage;
    private ProgressDialog loadingBar;
    private TextView TxtName;
    private TextView TxtEmail;
    private TextView TxtBio;
    private FirebaseAuth firebaseAuth;


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.perfil_fragment,container,false);
        imagen= (ImageView) v.findViewById(R.id.profilePicture);
        TxtName= (TextView) v.findViewById(R.id.txtName);
        TxtEmail= (TextView) v.findViewById(R.id.txtEmail);
        //TxtBio= (TextView) v.findViewById(R.id.txtBio);
        /* Define Your Functionality Here
           Find any view  => v.findViewById()
          Specifying Application Context in Fragment => getActivity() */

        firebaseAuth = FirebaseAuth.getInstance();
        if(firebaseAuth.getCurrentUser()!=null){

            FirebaseUser user = firebaseAuth.getCurrentUser();
            DatabaseReference currentUserDb = FirebaseDatabase.getInstance().getReference().child("Users");
            currentUserDb.child(firebaseAuth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    String name="Nombre: "+dataSnapshot.child("nombre").getValue().toString();
                    TxtName.setText(name);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });


            if(user.getEmail()!=null){

                String email= "Email: "+user.getEmail();

                TxtEmail.setText(email);

            }



        }


        return v;




    }








}
