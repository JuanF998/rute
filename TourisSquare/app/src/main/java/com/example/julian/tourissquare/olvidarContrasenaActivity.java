package com.example.julian.tourissquare;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class olvidarContrasenaActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText TextEmail;
    private ProgressDialog progressDialog;
    //Declaro un objeto firebaseAuth
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_olvidar_contrasena);
        firebaseAuth = FirebaseAuth.getInstance();
        TextEmail = (EditText) findViewById(R.id.TxtCorreo);
        Button btnEnviar = (Button) findViewById(R.id.BtnEnviar);
        btnEnviar.setOnClickListener(this);

        progressDialog = new ProgressDialog(this);
    }



    private void recuperarPassword(){
        final String email = TextEmail.getText().toString().trim();
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, "Se debe ingresar un email", Toast.LENGTH_LONG).show();


        }else{

            firebaseAuth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {

                    if(task.isSuccessful()){
                        Toast.makeText(olvidarContrasenaActivity.this, "Por favor revise su correo para recuperar la contraseña.", Toast.LENGTH_LONG).show();
                        Intent intencion = new Intent(getApplication(), MainActivity.class);
                        startActivity(intencion);
                    }else{

                        Toast.makeText(olvidarContrasenaActivity.this, "Un error ha ocurrido.", Toast.LENGTH_LONG).show();

                    }
                }
            });
        }

    }




    public void onClick(View view) {


        recuperarPassword();


    }
}
