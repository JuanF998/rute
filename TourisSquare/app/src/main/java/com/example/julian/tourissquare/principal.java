package com.example.julian.tourissquare;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class principal extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private TextView TxtEmail;
    private TextView TxtName;
    private FirebaseAuth firebaseAuth;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        TxtEmail=(TextView)findViewById(R.id.txtEmail);
        TxtName = (TextView) findViewById(R.id.txtName);
        firebaseAuth = FirebaseAuth.getInstance();
        if(firebaseAuth.getCurrentUser()!=null){

            FirebaseUser user = firebaseAuth.getCurrentUser();
            DatabaseReference currentUserDb = FirebaseDatabase.getInstance().getReference().child("Users");
            currentUserDb.child(firebaseAuth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    String name=dataSnapshot.child("nombre").getValue().toString();
                    TxtName.setText(name);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });


            if(user.getEmail()!=null){

                String email= user.getEmail();

                TxtEmail.setText(email);

            }

        }


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final String[] menus = {"Crear Plaza","Buscar Plaza","Perfil","Salir"};

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation);
        navigationView.setNavigationItemSelectedListener(this);
        ListView navlist = (ListView) findViewById(R.id.list);
        NavPanelListAdapter adapter = new NavPanelListAdapter(this,menus);
        navlist.setAdapter(adapter);
        navlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                String selectedMenu = menus[i];

                switch (selectedMenu) {

                    case "Crear Plaza":
                        crearPlazaFragment crearFragment = new crearPlazaFragment();
                        getSupportFragmentManager().beginTransaction()
                                .setCustomAnimations(R.anim.enter,R.anim.exit,R.anim.pop_enter, R.anim.pop_exit)
                                .replace(R.id.content_frame,crearFragment)
                                .addToBackStack(null)
                                .commit();
                        break;

                    case "Buscar Plaza":
                        buscarPlazaFragment buscarFragment = new buscarPlazaFragment();
                        getSupportFragmentManager().beginTransaction()
                                .setCustomAnimations(R.anim.enter,R.anim.exit,R.anim.pop_enter, R.anim.pop_exit)
                                .replace(R.id.content_frame,buscarFragment)
                                .addToBackStack(null)
                                .commit();
                        break;

                    case "Perfil":
                        perfilFragment perfilFragment = new perfilFragment();
                        getSupportFragmentManager().beginTransaction()
                                .setCustomAnimations(R.anim.enter,R.anim.exit,R.anim.pop_enter, R.anim.pop_exit)
                                .replace(R.id.content_frame,perfilFragment)
                                .addToBackStack(null)
                                .commit();
                        break;

                    case "Salir":

                        FirebaseAuth.getInstance().signOut();
                        Toast.makeText(principal.this, "Se ha cerrado la sesión" , Toast.LENGTH_LONG).show();
                        Intent intencion = new Intent(getApplication(), MainActivity.class);
                        startActivity(intencion);
                        break;

                    default:
                        break;



                }

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);


            }
        });


            }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
            crearPlazaFragment crearFagment = new crearPlazaFragment();
            getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.enter,R.anim.exit,R.anim.pop_enter, R.anim.pop_exit)
                    .replace(R.id.content_frame,crearFagment)
                    .addToBackStack(null)
                    .commit();
        } else if (id == R.id.nav_gallery) {
            buscarPlazaFragment buscarFragment = new buscarPlazaFragment();
            getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.enter,R.anim.exit,R.anim.pop_enter, R.anim.pop_exit)
                    .replace(R.id.content_frame,buscarFragment)
                    .addToBackStack(null)
                    .commit();

        } else if (id == R.id.nav_share) {
            perfilFragment perfilFragment = new perfilFragment();
            getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.enter,R.anim.exit,R.anim.pop_enter, R.anim.pop_exit)
                    .replace(R.id.content_frame,perfilFragment)
                    .addToBackStack(null)
                    .commit();

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
